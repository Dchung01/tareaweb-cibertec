﻿(function (cibertec) {

    cibertec.getModal = getModalContent;
    cibertec.closeModal = closeModal;
    return cibertec;

    function getModalContent(url) {
        $.get(url, function (data) {
            $('.modal-body').html(data);
        });
    }

    function closeModal(option) {
        $("button[data-dismiss='modal']").click();
        $('.modal-body').html("");
        console.log("Close Modal");
        modifyAlertsClassess(option);
    }

    function modifyAlertsClassess(option) {
        $('#createMessage').addClass('hidden');
        $('#editMessage').addClass('hidden');
        $('#deleteMessage').addClass('hidden');

        if (option === 'create') {
            $('#createMessage').removeClass('hidden');

            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Creación exitosa!',
                showConfirmButton: false,
                timer: 1500
            })
        }
        else if (option === 'edit') {
            $('#editMessage').removeClass('hidden');

            Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Edición exitosa!',
                showConfirmButton: false,
                timer: 1500
            })
        }
        else if (option === 'delete') {
            $('#deleteMessage').removeClass('hidden');

            Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Eliminación exitosa!',
                showConfirmButton: false,
                timer: 1500
            })

        }

        /*
        window.setTimeout(function () {
            $(".alert").alert('close');
        }, 5000);
        */

        window.setTimeout(function () {
            $(".alert").fadeTo(500, 0).slideUp(1000, function () {
                $(this).remove();
            });
        }, 5000);
            
    }
})(window.cibertec = window.cibertec || {});