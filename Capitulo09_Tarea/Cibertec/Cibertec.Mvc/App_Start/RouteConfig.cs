﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Cibertec.Mvc
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapMvcAttributeRoutes();

            /*MapRoute de Clientes*/
            /*routes.MapRoute(
                name: "SociosDeNegocio",
                url: "SociosDeNegocio/{action}/{id}",
                defaults: new {Controller = "Customer", action ="Index", id=UrlParameter.Optional}
                );
                */
            /*MapRoue de CRUD modificando el orden del "action" y "id" */
            /*routes.MapRoute(
                name: "DefaultCRUD",
                url: "{controller}/{id}/{action}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
                );
                */
            /*MapRoute Genérico por defecto*/
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
