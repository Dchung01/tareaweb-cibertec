﻿using Cibertec.Models;
using Cibertec.Mvc.Models;
using Cibertec.UnitOfWork;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace Cibertec.Mvc.Controllers
{
    public class AccountController : BaseController
    {
        public AccountController(ILog log, IUnitOfWork unit): base(log, unit)
        {

        }
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            return View(new UserViewModel { ReturnUrl = returnUrl });
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public ActionResult Login(UserViewModel user)
        {
            if (!ModelState.IsValid) return View(user);
            var validUser = _unit.Users.ValidateUser(user.Email, user.Password);
            if(validUser == null)
            {
                ModelState.AddModelError("Error", "Invalid email or password");
                return View(user);
            }

            var identity = new ClaimsIdentity(new[]{
                new Claim(ClaimTypes.Email, validUser.Email),
                //new Claim(ClaimTypes.Role, validUser.Roles),
                new Claim(ClaimTypes.Name, $"{validUser.FirstName} {validUser.LastName}"),
                new Claim(ClaimTypes.NameIdentifier, validUser.Id.ToString())
            }, "ApplicationCookie");

            var context = Request.GetOwinContext();
            var authManager = context.Authentication;

            authManager.SignIn(identity);

            return RedirectToLocal(user.ReturnUrl);
        }
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Register(RegisterUserViewModel userView)
        {
            if (!ModelState.IsValid) return View(userView);

            User user = new User
            {
                Email = userView.Email,
                FirstName = userView.FirstName,
                LastName = userView.LastName,
                Password = userView.Password
            };
            var validUser = _unit.Users.CreateUser(user);
            
            if (validUser == null)
            {
                ModelState.AddModelError("Error", "Invalid email or password");
                return View(userView); //cambiar variable user por userView
            }

            /*Si se quisiera autenticar automáticamente despues de la creación:
            var identity = new ClaimsIdentity(new[]{
                new Claim(ClaimTypes.Email, validUser.Email),
                new Claim(ClaimTypes.Role, validUser.Roles),
                new Claim(ClaimTypes.Name, $"{validUser.FirstName} {validUser.LastName}"),
                new Claim(ClaimTypes.NameIdentifier, validUser.Id.ToString())
            }, "ApplicationCookie");

            var context = Request.GetOwinContext();
            var authManager = context.Authentication;

            authManager.SignIn(identity);
            return RedirectToLocal(userView.ReturnUrl);
            */

            return RedirectToAction("Login", "Account");
        }

        public ActionResult Logout()
        {
            var context = Request.GetOwinContext();
            var authManager = context.Authentication;
            authManager.SignOut("ApplicationCookie");
            return RedirectToAction("Login", "Account");
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }
    }
}