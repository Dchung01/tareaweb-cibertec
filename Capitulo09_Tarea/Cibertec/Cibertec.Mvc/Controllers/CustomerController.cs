﻿using Cibertec.Models;
using Cibertec.Mvc.Action_Filters;
using Cibertec.Repositories.Dapper.Northwind;
using Cibertec.UnitOfWork;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Cibertec.Mvc.Controllers
{
    //[ErrorActionFilter]
    //[Authorize]
    [RoutePrefix("Customer")]
    public class CustomerController : BaseController
    {
        //private readonly IUnitOfWork _unit;
        public CustomerController(ILog log, IUnitOfWork unit): base(log,unit)
        {
            /* Sin inyeción de dependencia:
             * _unit = new NorthwindUnitOfWork(ConfigurationManager.
                                 ConnectionStrings["NorthwindConnection"].ToString());*/

            /*Con inyección de dependencia:*/
            //_unit = unit;
        }

        public ActionResult Error()
        {
            throw new System.Exception("Test error");
        }
        // GET: Customer
        public ActionResult Index()
        {
            _log.Info("Ejecución de Customer Controller Ok");
            return View(_unit.Customers.GetList());

            /*Si no se manejara el ActionFilter: */
            /*
            try
            {
                return View(_unit.Customers.GetList());
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                return RedirectToAction("PaginaError");
            }
            */
        }

        public /*ActionResult*/ PartialViewResult Create()
        {
            //Con Modal
            return PartialView("_Create", new Customers());

            //Sin Modal
            //return View();
        }
        //[HttpPost]
        //public ActionResult Create(Customers customer)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        _unit.Customers.Insert(customer);
        //        return RedirectToAction("Index");
        //    }
        //    //return View(customer);
        //    return PartialView("_Create", customer);
        //}

        public async Task<PartialViewResult> Create(Customers customers)
        {
            var httpClient = new HttpClient();
            var Credential = new Dictionary<string, string>
            {
                {"grant_type","password" },
                { "username","danielgustavo@hotmail.com"},
                { "password","abc_1234"}
            };

            var token = await httpClient.PostAsync("http://localhost:62263/token", new FormUrlEncodedContent(Credential));
            var tokenContent = token.Content.ReadAsStringAsync().Result;
            var tokerDictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(tokenContent);

            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokerDictionary["access_token"]);
            var json = await httpClient.GetStringAsync("http://localhost:62263/api/customer/Post"+customers);
            List<Customers> lstCreateCustomer = JsonConvert.DeserializeObject<List<Customers>>(json);

            return PartialView("_Create", lstCreateCustomer);
        }

        //public /*ActionResult*/ PartialViewResult Edit(String id)
        //{
        //    //return View(_unit.Customers.GetById(id));
        //    return PartialView("_Edit",_unit.Customers.GetById(id));
        //}

        public async Task<PartialViewResult> Edit(string id)
        {
            var httpClient = new HttpClient();
            var Credential = new Dictionary<string, string>
            {
                {"grant_type","password" },
                { "username","danielgustavo@hotmail.com"},
                { "password","abc_1234"}
            };

            var token = await httpClient.PostAsync("http://localhost:62263/token", new FormUrlEncodedContent(Credential));
            var tokenContent = token.Content.ReadAsStringAsync().Result;
            var tokerDictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(tokenContent);

            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokerDictionary["access_token"]);
            var json = await httpClient.GetStringAsync("http://localhost:62263/api/customer/get" + id);
            List<Customers> lstEditCustomer = JsonConvert.DeserializeObject<List<Customers>>(json);

            return PartialView("_Edit", lstEditCustomer);
        }

        [HttpPost]
        public ActionResult Edit(Customers customer)
        {
            if (_unit.Customers.Update(customer)) return RedirectToAction("Index");
            //return View(customer);
            return PartialView(customer);
        }

        //public /*ActionResult*/ PartialViewResult Delete(String id)
        //{
        //    //return View(_unit.Customers.GetById(id));
        //    return PartialView("_Delete",_unit.Customers.GetById(id));
        //}

        public async Task<PartialViewResult> Delete(string id)
        {
            var httpClient = new HttpClient();
            var Credential = new Dictionary<string, string>
            {
                {"grant_type","password" },
                { "username","danielgustavo@hotmail.com"},
                { "password","abc_1234"}
            };

            var token = await httpClient.PostAsync("http://localhost:62263/token", new FormUrlEncodedContent(Credential));
            var tokenContent = token.Content.ReadAsStringAsync().Result;
            var tokerDictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(tokenContent);

            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokerDictionary["access_token"]);
            var json = await httpClient.GetStringAsync("http://localhost:62263/api/customer/Delete" + id);
            List<Customers> lstDeleteCustomer = JsonConvert.DeserializeObject<List<Customers>>(json);

            return PartialView("_Delete", lstDeleteCustomer);
        }


        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeletePost(String id)
        {
            if (_unit.Customers.Delete(id)) return RedirectToAction("Index");
            //return View(_unit.Customers.GetById(id));
            return PartialView("_Delete", _unit.Customers.GetById(id));
        }


        //public /*ActionResult*/ PartialViewResult Details(String id)
        //{
        //    //return View(_unit.Customers.GetById(id));
        //    return PartialView("_Details",_unit.Customers.GetById(id));
        //}

        public async Task<PartialViewResult> Details(string id)
        {
            var httpClient = new HttpClient();
            var Credential = new Dictionary<string, string>
            {
                {"grant_type","password" },
                { "username","danielgustavo@hotmail.com"},
                { "password","abc_1234"}
            };

            var token = await httpClient.PostAsync("http://localhost:62263/token", new FormUrlEncodedContent(Credential));
            var tokenContent = token.Content.ReadAsStringAsync().Result;
            var tokerDictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(tokenContent);

            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokerDictionary["access_token"]);
            var json = await httpClient.GetStringAsync("http://localhost:62263/api/customer/get" + id);
            List<Customers> lstDeleteCustomer = JsonConvert.DeserializeObject<List<Customers>>(json);

            return PartialView("_Delete", lstDeleteCustomer);
        }


        [Route("List/{page:int}/{rows:int}")]
        //public PartialViewResult List(int page, int rows) //si no se consume servicios
        public async Task<PartialViewResult> List(int page, int rows)
        {
            /*
             * No se consume el servicios:
             *
           if (page <= 0 || rows <= 0) return PartialView(new List<Customers>());

           var startRecord = ((page - 1) * rows) + 1; 
           var endRecord = page * rows;
           return PartialView("_List", _unit.Customers.PagedList(startRecord, endRecord));
           */

            /*
             Si Consume por servicios:
             1.- solicitar token
             */
            var httpClient = new HttpClient();
            var Credential = new Dictionary<string, string>
            {
                {"grant_type","password" },
                { "username","danielgustavo@hotmail.com"},
                { "password","abc_1234"}
            };

            var token = await httpClient.PostAsync("http://localhost:62263/token",new FormUrlEncodedContent(Credential));
            var tokenContent = token.Content.ReadAsStringAsync().Result;
            var tokerDictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(tokenContent);

            //paso 2: consumir el servicio
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",tokerDictionary["access_token"]);
            var json= await httpClient.GetStringAsync("http://localhost:62263/api/customer/pagina/"+page+"/"+rows);

            List<Customers> lstCustomers = JsonConvert.DeserializeObject<List<Customers>>(json);

            return PartialView("_List", lstCustomers);

        }
        [Route("Count/{rows:int}")]
        public int CountPages(int rows)
        {
            var TotalRecords = _unit.Customers.Count();
            return TotalRecords % rows != 0 ? (TotalRecords / rows) + 1 : TotalRecords / rows;
        }
    }
}