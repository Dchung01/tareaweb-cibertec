﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Cibertec.Mvc.Hubs
{
    public class CustomerHub: Hub
    {
        static List<string> CustomerIds = new List<string>();
        //static List<int> CustomerAcceso = new List<int>();
        public void AddCustomerId(string id)
        {
            if (!CustomerIds.Contains(id))
            {
                CustomerIds.Add(id);
                //CustomerAcceso.Add(1);
            }
            Clients.All.customerStatus(CustomerIds);
        }
        public void RemoveCustomerId(string id)
        {
            if (CustomerIds.Contains(id)) CustomerIds.Remove(id);
            Clients.All.customerStatus(CustomerIds);
        }
        public override Task OnConnected()
        {
            return Clients.All.customerStatus(CustomerIds);
        }
        public void Message(string message)
        {
            Clients.All.getMessage(message);
        }
    }
}