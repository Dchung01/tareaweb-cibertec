﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Cibertec.Mvc.Models
{
    public class RegisterUserViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Correo electrónico")]
        public String Email { get; set; }
        [Required]
        public String FirstName { get; set; }
        public String LastName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "El número de caracteres de {0} de ser al menos {2}", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name ="Contraseña")]
        public String Password { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Confirmar Contraseña")]
        [Compare("Password", ErrorMessage = "La contraseña y la contraseña de confirmación no coinciden")]
        public String ConfirmPassword { get; set; }
    }
}