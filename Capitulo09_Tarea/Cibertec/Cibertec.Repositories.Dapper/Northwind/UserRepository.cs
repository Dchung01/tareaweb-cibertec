﻿using Cibertec.Models;
using Cibertec.Repositories.Northwind;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cibertec.Repositories.Dapper.Northwind
{
    public class UserRepository: Repository<User>, IUserRepository
    {
        public UserRepository(string connectionString): base(connectionString)
        {
        }

        public User ValidateUser(string email, string password)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@email", email);
                parameters.Add("@password", password);

                return connection.QueryFirstOrDefault<User>("dbo.UspValidateUser", parameters,
                    commandType: System.Data.CommandType.StoredProcedure);
            }
        }

        public User CreateUser(User user)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                String message = "";
                var parameters = new DynamicParameters();
                parameters.Add("@email", user.Email);
                parameters.Add("@password", user.Password);
                parameters.Add("@firstName", user.FirstName);
                parameters.Add("@lastName", user.LastName);
                parameters.Add("@ov_message_error", message);

                return connection.QueryFirstOrDefault<User>("dbo.UspCreateUser", parameters,
                    commandType: System.Data.CommandType.StoredProcedure);
            }
        }
    }
}