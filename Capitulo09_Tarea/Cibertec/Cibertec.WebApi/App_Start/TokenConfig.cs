﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Owin;
using System.Web.Http;
using Cibertec.UnitOfWork;
using Microsoft.Owin.Security.OAuth;
using Microsoft.Owin;
using System.Threading.Tasks;
using System.Security.Claims;

namespace Cibertec.WebApi.App_Start
{
    public class TokenConfig
    {
        public static void ConfigureAuth(IAppBuilder app, HttpConfiguration config)
        {
            var unitOfwork = (IUnitOfWork)config.DependencyResolver.GetService(typeof(IUnitOfWork));
            OAuthAuthorizationServerOptions oAuthServerOptions = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
                Provider = new SimpleAuthorizationServerProvider(unitOfwork)
            };

            app.UseOAuthAuthorizationServer(oAuthServerOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());

        }
   
        public class SimpleAuthorizationServerProvider: OAuthAuthorizationServerProvider
        {
            private readonly IUnitOfWork _unit;
            public  SimpleAuthorizationServerProvider(IUnitOfWork unit)
            {
                _unit = unit;
            }

            public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
            {
                await Task.Factory.StartNew(() => { context.Validated(); });
            }

            public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
            {
                await Task.Factory.StartNew(() =>
                {
                    var user = _unit.Users.ValidateUser(context.UserName, context.Password);
                    
                    if(user==null)
                    {
                        context.SetError("invalid_grant","Usuario o Password Incorrecto");
                        return;
                    }
                    var identity = new ClaimsIdentity(context.Options.AuthenticationType);
                    identity.AddClaim(new Claim("sub", context.UserName));
                    identity.AddClaim(new Claim("role", "user"));

                    context.Validated(identity);
                });
            }
        }
    }
}