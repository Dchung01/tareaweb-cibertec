﻿using Cibertec.Models;
using Cibertec.UnitOfWork;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Cibertec.WebApi.Controllers
{
    //[RoutePrefix("customer")]
    public class CustomerController : BaseController
    {
        public CustomerController(IUnitOfWork unit, ILog log) : base(unit, log)
        {
            //_log.Info($"{typeof(CustomerController)} en ejecucion");
        }

        [Route("error")]
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult CreateError(string id)
        {
            throw new System.Exception("Ha ocurrido un error");
        }

        //[Route("{id}")]
        public IHttpActionResult Get(string id)
        {
            if (id == "" || id == null) return BadRequest();
            return Ok(_unit.Customers.GetById(id));
        }
        //[Route("")]
        [HttpPost]
        public IHttpActionResult Post(Customers customer)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var id = _unit.Customers.Insert(customer);
            return Ok(new { id = id });
        }
        //[Route("")]
        [HttpPut]
        public IHttpActionResult Put(Customers customer)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            if (!_unit.Customers.Update(customer)) return BadRequest("Incorrect id");

            return Ok(new { status = true });
        }
        //[Route("{id}")]
        [HttpDelete]
        public IHttpActionResult Delete(string id)
        {
            if (id == "" || id == null) return BadRequest();
            var result = _unit.Customers.Delete(id);
            if (result) return Ok(new { delete = true });
            else return BadRequest("No se eliminó el cliente");
        }
        //[Route("list")]
        //[HttpGet]
        public IHttpActionResult GetList()
        {
            return Ok(_unit.Customers.GetList());
        }
        [Route("pagina/{page:int}/{rows:int}")]
        [HttpGet]
        /*Servicio a ser Conumido por proyecto mVC*/
        public IHttpActionResult pagina(int page, int rows)
        {
            if (page <= 0 || rows <= 0) return Ok(new List<Customers>());
 
            var startRecord = ((page - 1) * rows) + 1; 
            var endRecord = page * rows;
            return Ok(_unit.Customers.PagedList(startRecord, endRecord));

        }

    }
}
