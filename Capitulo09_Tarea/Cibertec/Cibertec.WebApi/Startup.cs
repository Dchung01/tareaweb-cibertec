﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using Cibertec.WebApi.App_Start;
using Cibertec.WebApi.Handlers;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Owin;

[assembly: OwinStartup(typeof(Cibertec.WebApi.Startup))]

namespace Cibertec.WebApi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            log4net.Config.XmlConfigurator.Configure();
            var log = log4net.LogManager.GetLogger(typeof(Startup));
            log.Debug("loggin is enabled");

            var config = new HttpConfiguration();
            config.Services.Replace(typeof(IExceptionHandler), new GlobalExceptionHandlers());

            DIConfig.ConfigureInjector(config);

            app.UseCors(CorsOptions.AllowAll);

            TokenConfig.ConfigureAuth(app, config);
            RouteConfig.Register(config);
            app.UseWebApi(config);
        }
    }
}
